using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SpawnManager : MonoBehaviour
{   
    [SerializeField]
    float mapSizeX = 20f;
    [SerializeField]
    float mapSizeY = 20f;
    float spawnHeight = 0.3f;
    float spawnDistanceFromWalls = 1.0f;

    [SerializeField]
    Transform spawnable;

    [SerializeField]
    bool canSpawn = true;

    void Update()
    {
        if(canSpawn)
        {
            SpawnCollectable();
        }
    }

    public void SetCanSpawn(bool _canSpawn)
    {
        canSpawn = _canSpawn;
    }

    void SpawnCollectable()
    {
        float xPos = UnityEngine.Random.Range(0 + spawnDistanceFromWalls, mapSizeX - spawnDistanceFromWalls);
        float yPos = UnityEngine.Random.Range(0 + spawnDistanceFromWalls, mapSizeY - spawnDistanceFromWalls);
        Vector3 spawnPosition = new Vector3(xPos, spawnHeight, yPos);
        if (!Physics.CheckSphere(spawnPosition, 1f, 7))
        {
            Instantiate(spawnable, spawnPosition, Quaternion.identity);
            SetCanSpawn(false);
        }
    }
}
