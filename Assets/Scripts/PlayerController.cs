using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerController : MonoBehaviour
{
    [SerializeField]
    float moveSpeed = 5f;
    [SerializeField]
    float turnSpeed = 5f;
    [SerializeField]
    SpawnManager spawnManager;
    [SerializeField]
    Rigidbody bodyPartPrefab;

    [SerializeField]
    DeathHandler menuHandler;

    [SerializeField]
    Score scoreManager;

    Rigidbody headRig;
    Vector3 currentEulerAngles;
    List<Vector3> followPoints;
    List<Rigidbody> bodyParts;

    //public int collected = 0;

    private void Start()
    {
        headRig = GetComponent<Rigidbody>();
        followPoints = new List<Vector3>();
        followPoints.Add(transform.position);
        bodyParts = new List<Rigidbody>();
        bodyParts.Add(headRig);
        StartCoroutine(CalculateFollowPoints());
    }

    void FixedUpdate()
    {
        Turn();
        MoveForward();
        MoveTowardsFollowPoint();
    }

    private void Turn()
    {
        float turnDirection = Input.GetAxis("Horizontal");
        currentEulerAngles += new Vector3(0, turnDirection, 0) * turnSpeed;
        headRig.rotation = Quaternion.Euler(currentEulerAngles);
    }

    void MoveForward()
    {
        headRig.velocity = headRig.transform.forward * moveSpeed;
    }

    void MoveTowardsFollowPoint()
    {
        if (bodyParts != null)
        {
            Vector3 direction;
            for (int i = bodyParts.Count - 1; i > 0; i--)
            {
                direction = followPoints[i] - bodyParts[i].position;
                bodyParts[i].velocity = direction * moveSpeed;
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Collectable")
        {
            Destroy(other.gameObject);
            spawnManager.SetCanSpawn(true);
            Grow();
            //collected++;
            scoreManager.IncreaseScore(10);
        }
        else if(other.tag == "Environment" || other.tag == "Player")
        {
            menuHandler.GameOverActions();
            Destroy(this.gameObject);
        }
    }

    void Grow()
    {
        Rigidbody bodyPart = Instantiate(bodyPartPrefab, bodyParts[bodyParts.Count - 1].position, transform.rotation);
        bodyParts.Add(bodyPart);
        followPoints.Add(bodyParts[bodyParts.Count - 1].position);
    }

    IEnumerator CalculateFollowPoints()
    {
        if (followPoints != null)
        {
            while (true)
            {
                for (int i = followPoints.Count - 1; i > 0; i--)
                {
                    followPoints[i] = followPoints[i - 1];
                }

                followPoints[0] = transform.position;
                yield return new WaitForSeconds(.05f);
            }
        }  
    } 



}
