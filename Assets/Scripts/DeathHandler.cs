using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class DeathHandler : MonoBehaviour
{
    public void GameOverActions()
    {
        gameObject.SetActive(true);
    }

    public void LoadCurrentScene()
    {
        Scene currentScene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(currentScene.name);
        gameObject.SetActive(false);
    }
}
