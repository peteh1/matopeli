using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Score : MonoBehaviour
{
    TextMeshProUGUI textMeshPro;
    //Text scoreText;
    [SerializeField]
    PlayerController playerController;

    int score = 0; 

    private void Start()
    {
        //scoreText = GetComponent<Text>();
        textMeshPro = GetComponent<TextMeshProUGUI>();
    }

    void Update()
    {
        //score = playerController.collected;
        //scoreText.text = $"Score: {score}";
        textMeshPro.text = $"Score: {score}";
    }

    public void IncreaseScore(int amount)
    {
        score += amount;
    }
}
